(function ($) {
  Drupal.behaviors.userActivityTime = function () {
    ping_time = new Date();
    
    if (loggedIn()) {
      
      $(document).on('mousemove keydown mousedown touchstart scroll', function () {
        if (checkPingTime()) {
          sendPing();
          ping_time = new Date();
        }
      });
      
      sendPing();
    }
    
    function checkPingTime() {
      var cur_ping_time = new Date();
      var ping_seconds = (cur_ping_time - ping_time) / 1000;
      if (ping_seconds > 60) {
        return true;
      } else {
        return false;
      }
    }
    
    function sendPing() {
      var postdata = {datetime: new Date().toLocaleString(), status: 1};
      $.post('/uat/ajax/track', postdata);
    }
    
    function loggedIn() {
      if ($("body.logged-in").length) {
       return true;
      }
      return false;
    }
    
  }
  
  $(document).ready(function () {
    Drupal.behaviors.userActivityTime();
  });
  
})(jQuery);