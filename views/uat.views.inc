<?php

/** 
 * @file 
 * 
 * Views integration for the User Activity Time module 
 */

/** 
 * Implements hook_views_data()
 */
function uat_views_data() {
  $data = array();

  $data['uat']['table']['group'] = t('User activity time');

  $data['uat']['table']['base'] = array(
    'title' => t('User activity time'),
    'help' => t('Contains user activity time records.'),
  );

  // The ID field
  $data['uat']['aid'] = array(
    'title' => t('ID'),
    'help' => t('The activity ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  
  // The Activity Start field
  $data['uat']['activity_start'] = array(
    'title' => t('Activity start'),
    'help' => t('The activity start datetime'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  // The Activity End field
  $data['uat']['activity_end'] = array(
    'title' => t('Activity end'),
    'help' => t('The activity end datetime'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['uat']['table']['join'] = array(
    'user' => array(
      'left_field' => 'uid',
      'field' => 'user_id',
    ),
  );
  
    
  // The custom Activity Range field
  $data['uat']['activity_range'] = array(
    'title' => t('Activity time range'),
    'help' => t('The user activity time range'),
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_uat_range_field',
    ),
  );

  // The User ID field
  $data['uat']['user_id'] = array(
    'title' => t('User ID'),
    'help' => t('The activity user ID.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'user_id',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
  );

  return $data;
  
}
