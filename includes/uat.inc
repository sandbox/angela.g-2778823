<?php

class UserActivityTime {

  protected $userID;
  
  public function __construct(array $values = array()) {
    global $user;
    $this->userID = $user->uid;
  }
  
  /**
   * Gets user last activity record
   *
   * @return
   *   An array with information about the user last activity record.
   */
  public function getLastActivity() {
    $query = db_select('uat', 'at')
        ->fields('at')
        ->range(0, 1)
        ->condition('at.user_id', $this->userID, '=')
        ->orderBy('at.activity_start', 'DESC');

    $result = $query
        ->execute()
        ->fetchAssoc();

    return $result;
  }

  /**
   * Gets user activity by start activity time
   *
   * @param $start_time
   *   Timestamp of user activity start datetime
   *
   * @return
   *   An array with information about the requested activity record.
   */
  public function getActivityByStartTime($start_time) {
    $query = db_select('uat', 'at')
        ->fields('at')
        ->condition('at.user_id', $this->userID, '=')
        ->condition('at.activity_start', $start_time, '=');

    $result = $query
        ->execute()
        ->fetchAssoc();

    return $result;
  }

  /**
   * Updates user activity record
   * 
   * @param $aid
   *   User activity id
   * @param $start_time
   *   Timestamp of user activity start datetime
   * @param $end_time
   *   Timestamp of user activity end datetime
   * 
   * @return
   *   A new UpdateQuery object
   */
  public function updateActivity($aid, $end_time, $start_time = null) {
    $update_fields = array(
      'activity_end' => $end_time,
    );
    if ($start_time) {
      $update_fields['activity_start'] = $start_time;
    }

    $query = db_update('uat')
        ->fields($update_fields)
        ->condition('aid', $aid, '=')
        ->execute();

    return $query;
  }
  
  /**
   * Creates user activity record
   * 
   * @param $start_time
   *   Timestamp of user activity start datetime
   * @param $end_time
   *   Timestamp of user activity end datetime
   * 
   * A new InsertQuery object
   */
  public function createActivity($start_time, $end_time) {
    $query = db_insert('uat')
        ->fields(array('user_id', 'activity_start', 'activity_end'))
        ->values(array(
          'user_id' => $this->userID,
          'activity_start' => $start_time,
          'activity_end' => $end_time,
        ))
        ->execute();

    return $query;
  }
}